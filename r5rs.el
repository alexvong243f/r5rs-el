;;; r5rs-el --- R5RS Scheme for Emacs -*- coding: utf-8; lexical-binding: t -*-
;;; Copyright © 2017, 2018 Alex Vong
;;;
;;; This file is part of r5rs-el.
;;;
;;; r5rs-el is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; r5rs-el is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with r5rs-el.  If not, see <http://www.gnu.org/licenses/>.


(require 'calc)
(require 'calc-ext)
(require 'cl-lib)


(cl-eval-when (compile) ; load itself during compilation
  (load-file "r5rs.el"))


;;; 4.1.2  Literal expressions
;;; CORE: quote


;;; 4.1.4  Procedures
;;; CORE: lambda


;;; 4.1.5  Conditionals
;;; CORE: if


;;; 4.1.6  Assignments
(defmacro r5-set! (var exp) ; `defalias' only works for procedures
  "Evaluate EXP and store the result to the value cell referred by VAR."
  `(setq ,var ,exp))


;;; 4.2.1  Conditionals
;;; CORE: and or
(defmacro r5-cond (clause &rest clauses)
  (r5-let ((temp (cl-gensym))
           (else-clause (if (null clauses) ; we cannot use `r5-null?' here
                            ''()
                          `(r5-cond ,@clauses))))
    (pcase-exhaustive clause
      (`(else ,result . ,results)
       `(r5-begin ,result ,@results))
      (`(,test => ,result)
       `(r5-let ((,temp ,test))
          (if ,temp
              (funcall ,result ,temp)
            ,else-clause)))
      (`(,test)
       `(r5-let ((,temp ,test))
          (if ,temp
              ,temp
            ,else-clause)))
      (`(,test ,result . ,results)
       `(if ,test
            (r5-begin ,result ,@results)
          ,else-clause)))))

(defmacro r5-case (key clause &rest clauses)
  (declare (indent defun))
  (pcase-exhaustive (reverse clauses)
    (`((else . ,exps) . ,rest)
     `(cl-case ,key
        ,@(reverse rest)
        (otherwise ,@exps)))
    (_
     `(cl-case ,key
        ,clause
        ,@clauses))))


;;; 4.2.2  Binding constructs
;;; CORE: let*


(defmacro r5-let (&rest args)
  (declare (indent defun))
  (pcase-exhaustive args
    ;; we cannot use `r5-symbol?' here
    (`(,(and tag (pred symbolp)) ,bindings ,form . ,forms)
     (r5-let ((names (r5-map #'car bindings))
              (vals (r5-map #'cadr bindings)))
       `(funcall (r5-letrec ((,tag (lambda ,names
                                     ,form
                                     ,@forms)))
                   ,tag)
                 ,@vals)))
    (_
     `(let ,@args))))


(defmacro r5-letrec (bindings form &rest forms)
  "Establish lexical bindings of (mutually recursive) procedures and values.

In order for mutually recursive procedures to be properly tail recursive,
the inital form of the bindings must `macroexpand' to a lambda expression
and the special form `r5-tail-call' must be used to indicate tail calls.

For example,

  (r5-letrec ((even? (lambda (n)
                       (if (r5-zero? n)
                           t
                         (r5-tail-call odd? (r5- n 1)))))
              (odd? (lambda (n)
                      (if (r5-zero? n)
                          '()
                        (r5-tail-call even? (r5- n 1))))))
    (funcall even? 1000))"

  (declare (indent defun))
  ;; Check if EXP expands to a lambda binding.
  (let* ((lambda-binding? (pcase-lambda (`(,_ ,exp))
                            (pcase-exhaustive (macroexpand exp)
                              (`(function (lambda . ,_))
                               t)
                              (_
                               '()))))

         ;; List of lambda bindings.
         (lambda-bindings (cl-remove-if-not lambda-binding? bindings))

         ;; List of non-lambda bindings.
         (non-lambda-bindings (cl-remove-if lambda-binding? bindings))

         ;; List of names of lambda bindings.
         (lambda-names (r5-map #'car lambda-bindings))

         ;; List of lambda expressions of lambda bindings.
         (lambda-exps (r5-map #'cadr lambda-bindings))

         ;; List of generated formal parameters.
         (lambda-formals (r5-map (lambda (_) (cl-gensym)) lambda-bindings))

         ;; Alist mapping names of lambda bindings to formal parameters.
         (lambda-name-formal-alist (r5-map #'cons lambda-names lambda-formals))

         ;; List of uninitialized bindings.
         (uninit-bindings (r5-map (pcase-lambda (`(,name ,_))
                                    (r5-let ((undefined (cl-gensym)))
                                      `(,name ',undefined)))
                                  bindings))

         ;; Assign variables with non-lambda expression initial forms.
         (assign-non-lambda-exps (r5-map (pcase-lambda (`(,name ,init))
                                           `(r5-set! ,name ,init))
                                         non-lambda-bindings))

         ;; Magic token used for tagging thunks.
         (magic (cl-gensym))

         ;; Self-application delta combinator.
         (delta (r5-let ((x (cl-gensym)))
                  `(lambda (,x) (funcall ,x ,x))))

         ;; Tagged-thunks-returning polyvariadic fix-point combinator Y*.
         (Y* (r5-let ((procs (cl-gensym))
                      (g (cl-gensym))
                      (f (cl-gensym))
                      (args (cl-gensym)))
               `(lambda (&rest ,procs)
                  (funcall ,delta
                           (lambda (,g)
                             (r5-map (lambda (,f)
                                       (lambda (&rest ,args)
                                         (cons ',magic
                                               (lambda ()
                                                 (apply (apply ,f
                                                               (funcall ,g ,g))
                                                        ,args)))))
                                     ,procs))))))

         ;; Proceduce s-exp to convert EXP to open recursive style.
         (open-recur
          (lambda (exp)
            (r5-let ((proc (cl-gensym))
                     (args (cl-gensym)))
              `(cl-macrolet
                   ((r5-tail-call
                     (,proc &rest ,args)
                     `(r5-begin
                       (ignore ,,proc)
                       (funcall ,(alist-get ,proc ',lambda-name-formal-alist)
                                ,@,args))))
                 (lambda ,lambda-formals
                   ,exp)))))

         ;; Procedure s-exp to tail-call-optimize EXP.
         (tco (lambda (exp)
                (r5-let ((args (cl-gensym))
                         (val (cl-gensym)))
                  `(lambda (&rest ,args)
                     (r5-do ((,val (apply ,exp ,args)
                                   (funcall (cdr ,val))))
                            ((pcase-exhaustive ,val
                               (`(,',magic . ,_)
                                '())
                               (_
                                t))
                             ,val))))))

         ;; Assign variables with lambda expressions.
         (assign-lambda-exps
          (r5-let ((tco-procs (cl-gensym))
                   (proc (cl-gensym)))
            `(r5-let ((,tco-procs
                       (r5-map (lambda (,proc)
                                 ,(funcall tco proc))
                               (funcall ,Y*
                                        ,@(r5-map (lambda (exp)
                                                    (funcall open-recur exp))
                                                  lambda-exps)))))
               ,@(r5-map (lambda (name k)
                           `(r5-set! ,name
                                     (r5-list-ref ,tco-procs ,k)))
                         lambda-names
                         (number-sequence 0 (length lambda-names)))))))

    `(r5-let ,uninit-bindings
       ,@assign-non-lambda-exps
       ,assign-lambda-exps
       ,form
       ,@forms)))


;;; 4.2.3  Sequencing
(defmacro r5-begin (exp &rest exps)
  "First evaluate EXP. Then evaluate EXPS sequentially.
Return the value of the last expression."
  `(progn ,exp ,@exps))


;;; 4.2.4  Iteration
(defmacro r5-do (bindings test-exps &rest commands)
  `(cl-do ,bindings
       ,test-exps
     ,@commands))


;;; 4.2.5  Delayed evaluation
(defmacro r5-delay (exp)
  "Create a promise to evaluate EXP without actually evaluating it.
The promise can be forced by `r5-force'."

  (r5-let ((result-ready? (cl-gensym))
           (result (cl-gensym))
           (x (cl-gensym)))
    `(r5-let ((,result-ready? '())
              (,result '()))
       (lambda ()
         (if ,result-ready?
             ,result
           (r5-let ((,x ,exp))
             (if ,result-ready?
                 ,result
               (r5-begin (r5-set! ,result-ready? t)
                         (r5-set! ,result ,x)
                         ,result))))))))


;;; 4.2.6  Quasiquotation
;;; CORE: quasiquote


;;; 4.3.1  Binding constructs for syntactic keywords
;;; OMIT: let-syntax letrec-syntax


;;; 5.2  Definitions
(defmacro r5-define (clause &rest forms)
  "Define either a top-level procedure or a global variable.

When being invoked as (r5-define (PROC ARGS ...) FORMS ... BODY),
define a (non-mutually) properly tail recursive procedure using `defun'.

For example,

  (r5-define (fact n accum)
    (if (r5-zero? n)
        accum
        (r5-tail-call fact (r5- n 1) (r5* n accum))))

See `r5-letrec' on how to define mutually properly tail recursive procedures
and the usage of `r5-tail-call'.


When being invoked as (r5-define VAR VAL),
define VAR to have value VAL using `r5-set!'."

  (declare (indent defun))
  (pcase-exhaustive clause
    (`(,var . ,formals)
     (letrec ((decl? (lambda (s-exp) ; we cannot use `r5-letrec' here
                       (pcase-exhaustive s-exp
                         (`(declare . ,_)
                          t)
                         (_
                          '()))))
              (tail-recursive? (lambda (s-exp)
                                 (pcase-exhaustive s-exp
                                   (`(r5-tail-call ,(pred (r5-eq? var)) . ,_)
                                    t)
                                   (`(,_ . ,_)
                                    (cl-some tail-recursive? s-exp))
                                   (_
                                    '())))))

       (pcase-let ((`(,doc-string* ,decl* ,body)
                    ;; we cannot use `r5-string?' here
                    (pcase-exhaustive forms
                      (`(,(and doc-string (pred stringp))
                         ,(and decl (guard (funcall decl? decl)))
                         .
                         ,body)
                       `((,doc-string) (,decl) ,body))
                      (`(,(and doc-string (pred stringp)) . ,body)
                       `((,doc-string) () ,body))
                      (`(,(and decl (guard (funcall decl? decl))) . ,body)
                       `(() (,decl) ,body))
                      (body
                       `(() () ,body)))))

         (if (funcall tail-recursive? body)
             ;; define properly tail-recursive procedure
             (r5-let ((args (cl-gensym)))
               `(defun ,var (&rest ,args)
                  ,@doc-string*
                  ,@decl*
                  (r5-letrec ((,var (lambda ,formals
                                      ,@body)))
                    (apply ,var ,args))))
           `(r5-begin (defun ,var ,formals ; define ordinary procedure
                        ,@doc-string*
                        ,@decl*
                        ,@body))))))

    (var ; define global variable
     `(r5-set! ,var ,@forms))))


;;; 5.3  Syntax definitions
;;; OMIT: define-syntax


;;; 6.1  Equivalence predicates
(defalias 'r5-eqv? #'eql)
(defalias 'r5-eq? #'eq)
(defalias 'r5-equal? #'equal)


;;; 6.2.1  Numerical types
;;; Emacs only has native support for real and integer.
;;; Calc package is needed to support complex and rational.


;;; 6.2.2  Exactness
;;; Emacs only has native support for inexact number,
;;; in the form of fixnum and flonum (hardware float).
;;; Calc package is needed to support exact number.
;;; Moroever, Calc package comes with its own flonum (software float).


;;; 6.2.5  Numerical operations
;;; NOTE: Many numerical procedures from Emcas Calc are tricky to wrap
;;;       because they may not produce the behaviour specified by R5RS.
;;;       For example, in Scheme, (= 1.0  1) => #t
;;;                               (max 3.9 4) => 4.0
;;;                               (complex? 1) => #t
;;;
;;; Currently, there are 2 convertion procedures, `fix-float' and `normalize'.
;;; Usually, `fix-float' suffices. When it is not, use `normalize'.
;;; This is determined experimentally.
(cl-macrolet ((fix-float (x)
                         (r5-let ((x* (cl-gensym)))
                           `(r5-let ((,x* ,x))
                              (if (floatp ,x*)
                                  (r5-string->number (r5-number->string ,x*))
                                ,x*))))

              (normalize (x)
                         `(math-normalize (fix-float ,x))))


  (r5-define (r5-number? x)
    "Return t if X is a number, () otherwise."
    (if (or (math-numberp x)
            (floatp x))
        t
      '()))

  (defalias 'r5-complex? #'r5-number?)

  (r5-define (r5-real? x)
    "Return t if X is a number with zero imaginary part, () otherwise."
    (and (r5-number? x)
         (r5-zero? (r5-imag-part x))))

  (defalias 'r5-rational? #'r5-real?)

  (r5-define (r5-integer? x)
    "Return t if X is a real number with integer value, () otherwise."
    (and (r5-real? x)
         (r5= x (r5-round x))))


  (r5-define (r5-exact? x)
    "Return t if X is a number but neither a native nor a Emacs Calc float,
() otherwise."
    (and (r5-number? x)
         (not (math-floatp x))
         (not (floatp x))))

  (r5-define (r5-inexact? x)
    "Return t if X is a native or Emacs Calc float, () otherwise."
    (or (math-floatp x)
        (floatp x)))


  (r5-define (r5= &rest nums)
    "Return t if NUMS are numerically equal, () otherwise."
    (pcase-exhaustive nums
      (`()
       t)
      (`(,head . ,rest)
       (r5-let ((z (normalize head)))
         (cl-every (lambda (x)
                     (math-equal (normalize x) z))
                   rest)))))

  (r5-define (r5< &rest nums)
    "Return t if NUMS are strictly monotonic increasing, () otherwise."
    (r5-let ((nums (r5-map (lambda (x)
                             (normalize x))
                           nums)))
      (pcase-exhaustive nums
        (`()
         t)
        (`(,_ . ,rest)
         (cl-every #'math-lessp nums rest)))))

  (r5-define (r5> &rest nums)
    "Return t if NUMS are strictly monotonic decreasing, () otherwise."
    (r5-let ((nums (r5-map (lambda (x)
                             (normalize x))
                           nums)))
      (pcase-exhaustive nums
        (`()
         t)
        (`(,_ . ,rest)
         (cl-every #'math-lessp rest nums)))))

  (r5-define (r5<= &rest nums)
    "Return t if NUMS are monotonic non-decreasing, () otherwise."
    (r5-let ((nums (r5-map (lambda (x)
                             (normalize x))
                           nums)))
      (pcase-exhaustive nums
        (`()
         t)
        (`(,_ . ,rest)
         (cl-every (lambda (x y)
                     (or (math-lessp x y)
                         (math-equal x y)))
                   nums
                   rest)))))

  (r5-define (r5>= &rest nums)
    "Return t if NUMS are monotonic non-increasing, () otherwise."
    (r5-let ((nums (r5-map (lambda (x)
                             (normalize x))
                           nums)))
      (pcase-exhaustive nums
        (`()
         t)
        (`(,_ . ,rest)
         (cl-every (lambda (x y)
                     (or (math-lessp x y)
                         (math-equal x y)))
                   rest
                   nums)))))


  (r5-define (r5-zero? z)
    "Return t if Z is numerically zero, () otherwise."
    (math-zerop (fix-float z)))

  (r5-define (r5-positive? x)
    "Return t if X is positive, () otherwise."
    (math-posp (normalize x)))

  (r5-define (r5-negative? x)
    "Return t if X is negative, () otherwise."
    (math-negp (normalize x)))

  (r5-define (r5-odd? n)
    "Return t if N == 1 mod 2, () otherwise."
    (r5= (r5-modulo n 2) 1))

  (r5-define (r5-even? n)
    "Return t if N == 0 mod 2, () otherwise."
    (r5-zero? (r5-modulo n 2)))

  (r5-define (r5-max x &rest xs)
    "Return the maximum of (CONS X XS). The returned number is inexact if any
number in (CONS X XS) is inexact, exact otherwise."
    (let* ((xs* (r5-map (lambda (x)
                          (normalize x))
                        (cons x xs)))
           (max (apply #'calcFunc-max xs*)))
      (if (and (r5-exact? max)
               (cl-some #'r5-inexact? xs*))
          (r5-exact->inexact max)
        max)))


  (r5-define (r5-min x &rest xs)
    "Return the minimum of (CONS X XS). The returned number is inexact if any
number in (CONS X XS) is inexact, exact otherwise."
    (let* ((xs* (r5-map (lambda (x)
                          (normalize x))
                        (cons x xs)))
           (min (apply #'calcFunc-min xs*)))
      (if (and (r5-exact? min)
               (cl-some #'r5-inexact? xs*))
          (r5-exact->inexact min)
        min)))


  (r5-define (r5+ &rest zs)
    "Return the sum of numbers in ZS, with 0 being the empty sum."
    (apply #'calcFunc-add
           (r5-map (lambda (z)
                     (fix-float z))
                   zs)))

  (r5-define (r5* &rest zs)
    "Return the product of numbers in ZS, with 1 being the empty product."
    (apply #'calcFunc-mul
           (r5-map (lambda (z)
                     (fix-float z))
                   zs)))


  (r5-define (r5- z &rest zs)
    "Return the additive inverse of Z if ZS is (), return the result of
subtracting Z by the sum of numbers in ZS otherwise."
    (pcase-exhaustive zs
      (`()
       (calcFunc-sub 0 (fix-float z)))
      (_
       (apply #'calcFunc-sub
              (r5-map (lambda (z)
                        (fix-float z))
                      (cons z zs))))))

  (r5-define (r5/ z &rest zs)
    "Return the multiplicative inverse if ZS is (), return the resulting of
dividing Z by the product of numbers in ZS otherwise."
    (let ((calc-prefer-frac t)) ; dynamic binding
      (pcase-exhaustive zs
        (`()
         (calcFunc-div 1 (fix-float z)))
        (_
         (r5-let ((zs* (cons z zs)))
           (apply #'calcFunc-div
                  (r5-map (lambda (z)
                            (fix-float z))
                          zs*)))))))


  (defalias 'r5-abs #'r5-magnitude)


  (r5-define (r5-quotient n1 n2)
    "Return the quotient of dividing N1 by N2."
    (r5-truncate (r5/ n1 n2)))

  (r5-define (r5-remainder n1 n2)
    "Return the remainder of dividing N1 by N2."
    (r5- n1 (r5* n2 (r5-quotient n1 n2))))

  (r5-define (r5-modulo n1 n2)
    "Return R where N1 == R mod N2 and between 0 and N2 but not equal to N2."
    (r5-let ((r (r5-remainder n1 n2)))
      (if (or (r5-zero? r)
              (and (r5>= n1 0) (r5>= n2 0))
              (and (r5<= n1 0) (r5<= n2 0)))
          r
        (+ r n2))))

  (r5-define (r5-gcd &rest nums)
    "Return the greatest common divisor of NUMS. The returned number is inexact
if any number in NUMS is inexact, exact otherwise."
    (let* ((nums (r5-map (lambda (x)
                           (normalize x))
                         nums))
           (gcd (cl-reduce #'calcFunc-gcd nums :initial-value 0)))
      (if (and (r5-exact? gcd)
               (cl-some #'r5-inexact? nums))
          (r5-exact->inexact gcd)
        gcd)))

  (r5-define (r5-lcm &rest nums)
    "Return the lowest common multiplier of NUMS. The returned number is
inexact if any number in NUMS is inexact, exact otherwise."
    (let* ((nums (r5-map (lambda (x)
                           (normalize x))
                         nums))
           (lcm (cl-reduce #'calcFunc-lcm nums :initial-value 1)))
      (if (and (r5-exact? lcm)
               (cl-some #'r5-inexact? nums))
          (r5-exact->inexact lcm)
        lcm)))

;;; FIXME: numerator denominator


  (r5-define (r5-floor x)
    "Round X towards negative infinify."
    (r5-let ((x (normalize x)))
      (if (r5-inexact? x)
          (calcFunc-ffloor x)
        (math-floor x))))

  (r5-define (r5-ceiling x)
    "Round X towards positive infinify."
    (r5-let ((x (normalize x)))
      (if (r5-inexact? x)
          (calcFunc-fceil x)
        (math-ceiling x))))

  (r5-define (r5-truncate x)
    "Round X towards zero."
    (r5-let ((x (fix-float x)))
      (if (r5-inexact? x)
          (calcFunc-ftrunc x)
        (math-trunc x))))

  (r5-define (r5-round x)
    "Round X towards the nearest integer."
    (r5-let ((x (normalize x)))
      (if (r5-inexact? x)
          (calcFunc-fround x)
        (math-round x))))


;;; FIXME: rationalize


  (r5-define (r5-exp z)
    "Compute the exponential of Z."
    (calcFunc-exp (fix-float z)))

  (r5-define (r5-log z)
    "Compute the (principal) logarithm of Z."
    (calcFunc-ln (fix-float z)))

  (r5-define (r5-sin z)
    "Compute the sine of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-sin (fix-float z))))

  (r5-define (r5-cos z)
    "Compute the cosine of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-cos (fix-float z))))

  (r5-define (r5-tan z)
    "Compute the tangent of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-tan (fix-float z))))

  (r5-define (r5-asin z)
    "Compute the (principal) inverse sine of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-arcsin (fix-float z))))

  (r5-define (r5-acos z)
    "Compute the (principal) inverse cosine of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-arccos (fix-float z))))

  (r5-define (r5-atan y &optional x)
    "Compute the inverse tangent of Z."
    (if (r5-null? x)
        (let ((calc-angle-mode 'rad)) ; dynamic binding
          (calcFunc-arctan (fix-float y)))
      (r5-angle (r5-make-rectangular x y))))


  (r5-define (r5-sqrt z)
    "Compute the (principal) square root of Z"
    (math-sqrt (fix-float z)))

  (r5-define (r5-expt z1 z2)
    "Compute Z1 to the power Z2."
    (if (r5-zero? z1)
        (if (r5-zero? z2)
            1
          0)
      (calcFunc-pow (fix-float z1)
                    (fix-float z2))))


  (r5-define (r5-make-rectangular x1 x2)
    "Return the complex number with real part X1 and imaginary part X2."
    (r5+ (fix-float x1)
         (math-imaginary (fix-float x2))))

  (r5-define (r5-make-polar x3 x4)
    "Return the complex number with magnitude X3 and argument X4."
    (r5* (fix-float x3)
         (r5-exp (math-imaginary (fix-float x4)))))

  (r5-define (r5-real-part z)
    "Return the real part of Z."
    (calcFunc-re (fix-float z)))

  (r5-define (r5-imag-part z)
    "Return the imaginary part of Z."
    (calcFunc-im (fix-float z)))

  (r5-define (r5-magnitude z)
    "Return the magnitude of Z."
    (math-abs (fix-float z)))

  (r5-define (r5-angle z)
    "Return the argument of Z."
    (let ((calc-angle-mode 'rad)) ; dynamic binding
      (calcFunc-arg (fix-float z))))


  (defalias 'r5-exact->inexact #'math-float
    "Convert number into an inexact one.")

  (r5-define (r5-inexact->exact z)
    "Convert Z into an exact number."
    (calcFunc-frac (fix-float z))))


;;; 6.2.6  Numerical input and output
(r5-define (r5-number->string z &optional radix)
  "Note that the following code only returns t for small integers.
The reliable way to test for number equality is by using `r5='.

  (r5-eqv? number
           (r5-string->number (r5-number->string number
                                                 radix)
                              radix))"

  (let ((calc-number-radix (if (r5-null? radix) ; dynamic binding
                               10
                             radix)))
    (math-format-number z)))

(r5-define (r5-string->number str &optional radix)
  "Tranform STR into Emacs Calc number.
Note that RADIX is ignored if there is radix annotation in STR."

  (cl-macrolet
      ((regex (type)
              (let* ((real (lambda (id-1 id-2 id-3)
                             `(seq (group-n ,id-1 (opt "-"))
                                   (opt (group-n ,id-2 (1+ digit)) "#")
                                   (group-n ,id-3 (1+ (not blank))))))
                     (sep (lambda (id)
                            `(group-n ,id (or "," ";"))))
                     (seq* (lambda (&rest s-exps)
                             `(seq bos
                                   ,@(cl-reduce (lambda (x y)
                                                  `(,@x ,y (0+ blank)))
                                                s-exps
                                                :initial-value '((0+ blank)))
                                   eos))))

                (rx-to-string (pcase-exhaustive type
                                (`cplx
                                 (funcall seq*
                                          "("
                                          (funcall real 1 2 3)
                                          (funcall sep 4)
                                          (funcall real 5 6 7)
                                          ")"))
                                (`real
                                 (funcall seq*
                                          (funcall real 1 2 3))))
                              t)))

       (repl (&rest args) ; local alias
             `(replace-regexp-in-string ,@args))
       (string-null? (str)
                     `(r5-string=? ,str "")))

    (r5-let ((prefix (if (r5-null? radix)
                         ""
                       (r5-string-append (number-to-string radix) "#"))))
      (r5-cond ((string-match-p (regex cplx) str)
                (r5-let
                  ((str*
                    (if (and (string-null? (repl (regex cplx) "\\2" str))
                             (string-null? (repl (regex cplx) "\\6" str)))
                        (repl (regex cplx)
                              (r5-string-append "(\\1" prefix "\\3\\4\\5"
                                                prefix "\\7)")
                              str)
                      str)))
                  (math-read-expr str*)))
               ((string-match-p (regex real) str)
                (r5-let ((str* (if (string-null? (repl (regex real) "\\2" str))
                                   (repl (regex real)
                                         (r5-string-append "\\1" prefix "\\3")
                                         str)
                                 str)))
                  (math-read-number str*)))))))


;;; 6.3.2  Pairs and lists
;;; CORE: cons car cdr caar cadr cdar cddr list length append reverse
;;;       memq member assq assoc
(defalias 'r5-pair? #'consp)
(defalias 'r5-set-car! #'setcar)
(defalias 'r5-set-cdr! #'setcdr)


(defalias 'r5-caaar #'cl-caaar)
(defalias 'r5-caadr #'cl-caadr)
(defalias 'r5-cadar #'cl-cadar)
(defalias 'r5-caddr #'cl-caddr)
(defalias 'r5-cdaar #'cl-cdaar)
(defalias 'r5-cdadr #'cl-cdadr)
(defalias 'r5-cddar #'cl-cddar)
(defalias 'r5-cdddr #'cl-cdddr)
(defalias 'r5-caaaar #'cl-caaaar)
(defalias 'r5-caaadr #'cl-caaadr)
(defalias 'r5-caadar #'cl-caadar)
(defalias 'r5-caaddr #'cl-caaddr)
(defalias 'r5-cadaar #'cl-cadaar)
(defalias 'r5-cadadr #'cl-cadadr)
(defalias 'r5-caddar #'cl-caddar)
(defalias 'r5-cadddr #'cl-cadddr)
(defalias 'r5-cdaaar #'cl-cdaaar)
(defalias 'r5-cdaadr #'cl-cdaadr)
(defalias 'r5-cdadar #'cl-cdadar)
(defalias 'r5-cdaddr #'cl-cdaddr)
(defalias 'r5-cddaar #'cl-cddaar)
(defalias 'r5-cddadr #'cl-cddadr)
(defalias 'r5-cdddar #'cl-cdddar)
(defalias 'r5-cddddr #'cl-cddddr)


(defalias 'r5-null? #'null)

(r5-define (r5-list? x)
  "Return t is X is a proper list, () otherwise."
  (and (listp x)
       (r5-null? (cdr (last x)))))

(r5-define (r5-list-tail ls k)
  "Return LS with its first Kth elements removed."
  (nthcdr k ls))

(r5-define (r5-list-ref ls k)
  "Return the Kth element of LS. "
  (nth k ls))

(defalias 'r5-memv #'memql)
(defalias 'r5-assv #'cl-assoc)


;;; 6.3.3  Symbols
(defalias 'r5-symbol? #'symbolp)
(defalias 'r5-symbol->string #'symbol-name)
(defalias 'r5-string->symbol #'intern)


;;; 6.3.4  Characters
(defalias 'r5-char? #'characterp)


(r5-define (r5-char=? char1 char2)
  (r5-string=? (string char1) (string char2)))

(r5-define (r5-char<? char1 char2)
  (r5-string<? (string char1) (string char2)))

(r5-define (r5-char>? char1 char2)
  (r5-string>? (string char1) (string char2)))

(r5-define (r5-char<=? char1 char2)
  (r5-string<=? (string char1) (string char2)))

(r5-define (r5-char>=? char1 char2)
  (r5-string>=? (string char1) (string char2)))


(r5-define (r5-char-ci=? char1 char2)
  (r5-string-ci=? (string char1) (string char2)))

(r5-define (r5-char-ci<? char1 char2)
  (r5-string-ci<? (string char1) (string char2)))

(r5-define (r5-char-ci>? char1 char2)
  (r5-string-ci>? (string char1) (string char2)))

(r5-define (r5-char-ci<=? char1 char2)
  (r5-string-ci<=? (string char1) (string char2)))

(r5-define (r5-char-ci>=? char1 char2)
  (r5-string-ci>=? (string char1) (string char2)))


(r5-define (r5-char-alphabetic? char)
  (if (string-match-p (rx bos alphabetic eos) (string char))
      t
    '()))

(r5-define (r5-char-numeric? char)
  (if (string-match-p (rx bos numeric eos) (string char))
      t
    '()))

(r5-define (r5-char-whitespace? char)
  (if (string-match-p (rx bos whitespace eos) (string char))
      t
    '()))

(r5-define (r5-char-upper-case? char)
  (if (string-match-p (rx bos upper-case eos) (string char))
      t
    '()))

(r5-define (r5-char-lower-case? char)
  (if (string-match-p (rx bos lower-case eos) (string char))
      t
    '()))


(defalias 'r5-char->integer #'identity)
(defalias 'r5-integer->char #'identity)


(defalias 'r5-char-upcase #'upcase)
(defalias 'r5-char-downcase #'downcase)


;;; 6.3.5  Strings
;;; CORE: string substring
(defalias 'r5-string? #'stringp)

(r5-define (r5-make-string k &optional char)
  "Create a string of length K and optionally initialize with FILL.
If FILL is omitted, it will be initialized with  ."

  (make-string k (if (r5-null? char)
                     0
                   char)))

(defalias 'r5-string-length #'length)


(defalias 'r5-string-ref #'aref)
(defalias 'r5-string-set! #'aset)


(defalias 'r5-string=? #'string=)

(r5-define (r5-string-ci=? s1 s2)
  (r5-eq? (compare-strings s1 '() '() s2 '() '() t)
          t))


(defalias 'r5-string<? #'string<)

(r5-define (r5-string>? s1 s2)
  (r5-string<? s2 s1))

(r5-define (r5-string<=? s1 s2)
  (not (r5-string>? s1 s2)))

(r5-define (r5-string>=? s1 s2)
  (not (r5-string<? s1 s2)))

(r5-define (r5-string-ci<? s1 s2)
  (not (r5-string-ci>=? s1 s2)))

(r5-define (r5-string-ci>? s1 s2)
  (not (r5-string-ci<=? s1 s2)))

(r5-define (r5-string-ci<=? s1 s2)
  (r5-let ((val (compare-strings s1 '() '() s2 '() '() t)))
    (or (r5-eq? val t)
        (r5< val 0))))

(r5-define (r5-string-ci>=? s1 s2)
  (r5-string-ci<=? s2 s1))


(defalias 'r5-string-append #'concat)


(defalias 'r5-string->list #'string-to-list)
(defalias 'r5-list->string #'concat)


(defalias 'r5-string-copy #'copy-sequence)
(defalias 'r5-string-fill! #'fillarray)


;;; 6.3.6  Vectors
;;; CORE: vector
(defalias 'r5-vector? #'vectorp)

(r5-define (r5-make-vector k &optional fill)
  "Create a vector of length K and optionally initialize with FILL.
If FILL is omitted, it will be initialized with ()."

  (make-vector k fill))

(defalias 'r5-vector-length #'length)


(defalias 'r5-vector-ref #'aref)
(defalias 'r5-vector-set! #'aset)


(r5-define (r5-vector->list vec)
  "Convert VEC to list."
  (append vec '()))

(defalias 'r5-list->vector #'vconcat)


(defalias 'r5-vector-fill! #'fillarray)


;;; 6.4  Control features
;;; CORE: apply
;;; OMIT: call-with-current-continuation
(defalias 'r5-procedure? #'functionp)


(defalias 'r5-map #'cl-mapcar)
(defalias 'r5-for-each #'cl-mapc)


(r5-define (r5-force promise)
  "Force evaluation of PROMISE returned by `r5-delay'.
The result is memoized, subsequent forcing will always return the same result."

  (funcall promise))


(cl-macrolet ((mrvs ()
                    (r5-let ((args (cl-gensym))
                             (arg (cl-gensym))
                             (magic (cl-gentemp)) ; interned magic symbol
                             (producer (cl-gensym))
                             (consumer (cl-gensym))
                             (x (cl-gensym))
                             (rest (cl-gensym)))

                      `(r5-begin
                        (r5-define (r5-values &rest ,args)
                          "Create a mutiple-values from ARGS.
The multiple-values can be passed as arguments using `r5-call-with-values'."
                          (pcase-exhaustive ,args
                            (`(,,arg)
                             ,arg)
                            (_
                             (cons ',magic ,args))))

                        (r5-define (r5-call-with-values ,producer ,consumer)
                          "Call CONSUMER with multiple-values from PRODUCER.
The multiple-values can be created using `r5-values'."
                          (declare (indent 1))
                          (r5-let ((,x (funcall ,producer)))
                            (pcase-exhaustive ,x
                              (`(,',magic . ,,rest)
                               (apply ,consumer ,rest))
                              (_
                               (funcall ,consumer ,x)))))

                        (unintern ',magic '()))))) ; unintern the magic symbol
  (mrvs))


(r5-define (r5-dynamic-wind before thunk after)
  (unwind-protect (r5-begin (funcall before)
                            (funcall thunk))
    (funcall after)))


;;; 6.5  Eval
;;; CORE: eval
;;; OMIT: scheme-report-environment null-environment
(r5-define (r5-interaction-environment)
  "Use as the second argument for `eval'.

For example,
  (eval '(r5+ 1 1) (r5-interaction-environment))"

  t)


;;; 6.6.1  Ports
;;; We identify ports with Emacs streams since they are conceptually similar.
;;; However, some procedures does not support all the stream types.
(r5-define (r5-call-with-input-file filename proc)
  "Open an input port associated to FILENAME using `r5-open-input-file',
call PROC with the opened port and return the result. The opened port is closed
by `r5-close-input-port' after PROC returned."

  (declare (indent 1))
  (r5-let ((port (r5-open-input-file filename)))
    (unwind-protect (funcall proc port)
      (r5-close-input-port port))))

(r5-define (r5-call-with-output-file filename proc)
  "Open an output port associated to FILENAME using `r5-open-output-file',
call PROC with the opened port and return the result. The opened port is closed
by `r5-close-output-port' after PROC returned."

  (declare (indent 1))
  (r5-let ((port (r5-open-output-file filename)))
    (unwind-protect (funcall proc port)
      (r5-close-output-port port))))


(r5-define (r5-input-port? x)
  "Return t if X is certainly an input port, () otherwise. This means X is a
buffer, a marker with an underlying buffer, a string or t (which represents
minibuffer or stdin)."

  (or (bufferp x)
      (and (markerp x)
           (r5-tail-call r5-input-port?
                         (marker-buffer x)))
      (r5-string? x)
      (r5-eq? x t)))

(r5-define (r5-output-port? x)
  "Return t if X is certainly an output port, () otherwise. This means X is a
writable buffer, a marker with an underlying writable buffer or t (which
represents echo area or stdout)."

  (or (and (bufferp x)
           (not (with-current-buffer x
                  buffer-read-only)))
      (and (markerp x)
           (r5-tail-call r5-output-port?
                         (marker-buffer x)))
      (r5-eq? x t)))


(r5-define (r5-current-input-port)
  "Return the stream representing the current input port."
  standard-input)

(r5-define (r5-current-output-port)
  "Return the stream representing the current output port."
  standard-output)


(r5-define (r5-with-input-from-file filename thunk)
  "Open an input port associated to FILENAME using `r5-open-input-file', force
THUNK with current input port set to the opened port and return the result. The
opened port is closed by `r5-close-input-port' after THUNK returned."

  (declare (indent 1))
  (r5-let ((port (r5-open-input-file filename)))
    (unwind-protect (let ((standard-input port)) ; dynamic binding
                      (funcall thunk))
      (r5-close-input-port port))))

(r5-define (r5-with-output-to-file filename thunk)
  "Open an output port associated to FILENAME using `r5-open-output-file',
force THUNK with current output port set to the opened port and return the
result. The opened port is closed by `r5-close-output-port' after THUNK
returned."

  (declare (indent 1))
  (r5-let ((port (r5-open-output-file filename)))
    (unwind-protect (let ((standard-output port)) ; dynamic binding
                      (funcall thunk))
      (r5-close-output-port port))))


(r5-define (r5-open-input-file filename)
  "Open an input port associated to FILENAME.

Note that it should be closed by `r5-close-input-port' after use to avoid
resource leak."

  (r5-let ((buf (create-file-buffer filename)))
    (condition-case err
        (with-current-buffer buf
          (insert-file-contents-literally filename)
          (r5-set! buffer-read-only t)
          buf)
      (error (r5-close-input-port buf)
             (pcase-let ((`(,err-sym . ,data) err))
               (signal err-sym data))))))

(r5-define (r5-open-output-file filename)
  "Open an output port associated to FILENAME.

Note that it must be closed by `r5-close-output-port' after use for the changes
to be visible on the filesystem and to avoid resource leak."

  (r5-let ((buf (create-file-buffer filename)))
    (condition-case err
        (with-current-buffer buf
          (set-visited-file-name filename t)
          buf)
      (error (r5-close-output-port buf)
             (pcase-let ((`(,err-sym . ,data) err))
               (signal err-sym data))))))


(r5-define (r5-close-input-port port)
  "Close input port PORT if it is a buffer or a marker with an underlying
buffer, do nothing otherwise."

  (r5-cond ((bufferp port)
            (with-current-buffer port
              (unwind-protect (set-buffer-modified-p '())
                (kill-buffer port))))
           ((markerp port)
            (r5-tail-call r5-close-input-port
                          (marker-buffer port)))
           (else
            '())))


(r5-define (r5-close-output-port port)
  "Flush and close output port PORT if it is a buffer or a marker with an
underlying buffer, do nothing otherwise."

  (r5-cond ((bufferp port)
            (with-current-buffer port
              (unwind-protect (basic-save-buffer)
                (unwind-protect (set-buffer-modified-p '())
                  (kill-buffer port)))))
           ((markerp port)
            (r5-tail-call r5-close-output-port
                          (marker-buffer port)))
           (else
            '())))


;;; 6.6.2  Input
;;; CORE: read
;;; All negative integers are considered to be end-of-file.
(cl-macrolet ((eof () -1)
              (string-null? (str)
                            `(r5-string=? ,str "")))

  (r5-define (r5-read-char &optional port)
    "Return a character from PORT by consuming it if the end of file has not
been reached, an end-of-file otherwise. If PORT is omitted, the port returned
by `r5-current-input-port' is used.

Note that string port is not supported because it is impossible to consume a
character from it.

See `r5-peek-char' for returning a character from port without consuming it."

    (r5-let ((port (if (r5-null? port)
                       (r5-current-input-port)
                     port)))
      (r5-cond ((bufferp port)
                (with-current-buffer port
                  (r5-let ((chr (char-after (point))))
                    (if (r5-null? chr)
                        (eof)
                      (r5-begin (forward-char)
                                chr)))))
               ((markerp port)
                (with-current-buffer (marker-buffer port)
                  (r5-let ((chr (char-after port)))
                    (if (r5-null? chr)
                        (eof)
                      (r5-begin (set-marker port
                                            (r5+ (marker-position port) 1))
                                chr)))))
               ((r5-procedure? port)
                (funcall port))
               ((r5-eq? port t)
                (read-char)))))

  (r5-define (r5-peek-char &optional port)
    "Return a character from PORT without consuming it if the end of file has
not been reached, an end-of-file otherwise. If PORT is omitted, the port
returned by `r5-current-input-port' is used.

See `r5-read-char' for returning a character from port by consuming it."

    (r5-let ((port (if (r5-null? port)
                       (r5-current-input-port)
                     port)))
      (r5-cond ((bufferp port)
                (with-current-buffer port
                  (r5-let ((chr (char-after (point))))
                    (if (r5-null? chr)
                        (eof)
                      chr))))
               ((markerp port)
                (with-current-buffer (marker-buffer port)
                  (r5-let ((chr (char-after port)))
                    (if (r5-null? chr)
                        (eof)
                      chr))))
               ((r5-string? port)
                (if (string-null? port)
                    (eof)
                  (r5-string-ref port 0)))
               ((r5-procedure? port)
                (r5-let ((chr (funcall port)))
                  (if (r5-eof-object? chr)
                      (eof)
                    (r5-begin (funcall port chr)
                              chr))))
               ((r5-eq? port t)
                (r5-let ((chr (read-char)))
                  (r5-set! unread-command-events
                           (cons chr unread-command-events))
                  chr)))))

  (r5-define (r5-eof-object? x)
    "Return t if X is an end-of-file, () otherwise."
    (and (r5-integer? x)
         (r5-negative? x)))

  (r5-define (r5-char-ready? &optional port)
    "Return t if character can certainly be returned from PORT without
blocking, () otherwise. This means PORT is a buffer or a marker with an
underlying buffer.

If PORT is omitted, the port returned by `r5-current-input-port' is used. "

    (r5-let ((port (if (r5-null? port)
                       (r5-current-input-port)
                     port)))
      (or (bufferp port)
          (and (markerp port)
               (r5-tail-call r5-char-ready?
                             (marker-buffer port)))
          (r5-string? port)))))


;;; 6.6.3  Output
;;; CORE: write-char
(defalias 'r5-write #'prin1)
(defalias 'r5-display #'princ)
(defalias 'r5-newline #'terpri)


;;; 6.6.4  System interface
;;; CORE: load
;;; OMIT: transcript-on transcript-off


(provide 'r5rs)
